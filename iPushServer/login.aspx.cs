﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using General;

namespace iPushServer
{
    public partial class login : System.Web.UI.Page
    {
        User U = new User();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["a"] == "out")
            {
                LogOut();
            }

        }

        void LogOut()
        {
            Session.Abandon();
            Session.Clear();
            Response.Redirect("login.aspx");
        }

        protected void btnLogin_OnClick(object sender, EventArgs e)
        {
            if (U.Authenticate(UserNameTextBox.Text, PasswordTextBox.Text))
            {
                Response.Redirect("Default.aspx"); 
            }
            else
            {
                ScriptLiteral.Text = string.Format("<Script>DisplayValidator('{0}','error','password');</Script>", Service.GetMessage("WrongAuth"));
            }
        }
    }
}