﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="iPushServer.Default" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.8.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="form">
        <div class="text">
            <label class="up" style="width: 100%;">Choose a device:</label>
            <div style="float: left;">
                <dx:ASPxComboBox ID="DevicesComboBox" runat="server" DataSourceID="DevicesDataSource" ValueField="DToken" TextField="DName"></dx:ASPxComboBox>
            </div>
        </div>
        <div class="text">
            <label class="up" style="width: 100%;">Please enter push notification text:</label>
            <asp:TextBox runat="server" TextMode="MultiLine" Width="400" MaxLength="200" CssClass="input" ID="iPushTextBox"></asp:TextBox>
            <asp:Button runat="server" ID="btnSend" OnClick="btnSend_OnClick" CssClass="button" Text="Send"/>
            <asp:Literal runat="server" ID="MsgLiteral"></asp:Literal>
        </div>
        <asp:ObjectDataSource ID="DevicesDataSource" runat="server" SelectMethod="List_Devices" TypeName="General.Device">
            <SelectParameters>
                <asp:Parameter Name="isActive" Type="Boolean" />
            </SelectParameters>
        </asp:ObjectDataSource>
    </div>
</asp:Content>
