﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="iPushServer.login" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="centered">
        <img src="img/logo.png" />
        <div class="text">
            <label class="b up">User Name</label>
            <asp:TextBox runat="server" ID="UserNameTextBox" placeholder="USER NAME" CssClass="input"></asp:TextBox>
             <div class="validator up" id="username"></div>
        </div>
        <div class="text">
            <label class="b up">Password</label>
            <asp:TextBox runat="server" ID="PasswordTextBox" CssClass="input" TextMode="Password" placeholder="PASSWORD"></asp:TextBox>
            <div class="validator up" id="password"></div>
        </div>
        <asp:Button runat="server" ID="btnLogin" CssClass="button" Text="SAVE" OnClick="btnLogin_OnClick"/>
    </div>
    <asp:Literal runat="server" ID="ScriptLiteral"></asp:Literal>
</asp:Content>
