﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace iPushServer
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSend_OnClick(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(iPushTextBox.Text))
            {
                General.PushNotification.Send(DevicesComboBox.Value.ToString(),iPushTextBox.Text);
                iPushTextBox.Text = "";
                MsgLiteral.Text = "Your notification sent..";
            }
        }
    }
}