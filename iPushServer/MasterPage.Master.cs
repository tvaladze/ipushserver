﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using General;

namespace iPushServer
{
    public partial class MasterPage : System.Web.UI.MasterPage
    {
        public string Root;
        public User UserObject { set; get; }
        public string FileName;

        protected void Page_Load(object sender, EventArgs e)
        {
            InitPage();
            SetNoCache();
        }

        void InitPage()
        {
            Root = "http://" + Request.Url.Host + Request.ApplicationPath;
            Root += Root.EndsWith("/") ? "" : "/";

            HiddenFieldsLiteral.Text = string.Format("<input type='hidden' id='RootPathHiddenField' value='{0}' />", Root);

            FileName = Request.ApplicationPath.Length > 1 ? Request.Path.Replace(Request.ApplicationPath + "/", string.Empty) : Request.Path;
            FileName = (FileName.StartsWith("/") ? FileName : "/" + FileName);

            UserObject = new User();

            if (!UserObject.GetAuthorizedCredentials())
            {
                if (FileName != "/login.aspx")
                {
                    Response.Redirect("~/login.aspx");
                }
            }

        }

        void SetNoCache()
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetExpires(DateTime.Now.AddHours(-1));
            Response.Cache.SetNoStore();
            Response.AppendHeader("Pragma", "no-cache");
        }

    }
}