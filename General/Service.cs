﻿using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using SystemBase;

namespace General
{

    public class Service : ObjectBase
    {
        /// <summary>
        /// Gets Error message by name from Resources
        /// </summary>
        /// <param name="MessageName"></param>
        /// <returns></returns>
        public static string GetMessage(string MessageName)
        {
            return Properties.Resources.ResourceManager.GetString(MessageName);
        }

        public static DBGeneralDataContext GetDBGeneralDataContext()
        {
            return new DBGeneralDataContext(ConfigurationManager.ConnectionStrings["DBConnectionString"].ConnectionString);
        }
    }

    public class Device : ObjectBase
    {
        #region Properties
            public string DToken { get; set; }
            public string DName { get; set; }
        #endregion

        #region Methods

            public List<Device> List_Devices(bool? isActive=null)
            {
                return TryToGetList<Device>("", delegate()
                {
                    DBGeneralDataContext db = Service.GetDBGeneralDataContext();
                    return db.List_Devices(isActive).Select(s => new Device
                    {
                        DToken = s.DToken,
                        DName = s.DName
                    }).ToList();

                });
            }

        #endregion


    }

    public class User : ObjectBase
    {
        #region Properties

        //public long ID { get; set; }
        //public string UserName { get; set; }
        //public string Email { get; set; }
        //public string Password { get; set; }
        //public string FName { get; set; }
        //public string LName { get; set; }
        //public string FLName { get; set; }
        //public int? RoleID { get; set; }
        //public string RoleName { get; set; }
        public bool? IsAuthorized { get; set; }
        public bool? IsAdmin { get; set; }
        #endregion

        #region Methods

        public bool Authenticate(string Username, string Password)
        {
            return TryToReturn<bool>("Authenticate(Username = " + Username + ", Password = " + Password + ")",
                delegate()
                {
                   //DBClassesDataContext db = ConnectionFactory.GetDBGeneralDataContext();
                   //string pwd = Password.MD5();
                   //var U = db.Authorise(Username, pwd).DefaultIfEmpty().Single();
                   // if (U != null)
                    if (Password=="asdf")
                    {
                        Hashtable ClientTable = new Hashtable();
                        ClientTable["IsAuthorized"] = true;
                       //ClientTable["ID"] = U.ID;
                       //ClientTable["IsAdmin"] = U.UserName == "admin";
                       //ClientTable["UserName"] = U.UserName;
                       //ClientTable["Email"] = U.Email;
                       //ClientTable["Password"] = Password;
                       //ClientTable["Fname"] = U.FName;
                       //ClientTable["Lname"] = U.Lname;
                       //ClientTable["RoleID"] = U.RoleID;
                       //ClientTable["RoleName"] = U.RoleName;

                        System.Web.HttpContext.Current.Session["UserInfo"] = ClientTable;
                        return true;
                    }
                    return false;
                });
        }

       public bool GetAuthorizedCredentials()
        {
            try
            {
                Hashtable ClientTable = (Hashtable)System.Web.HttpContext.Current.Session["UserInfo"];
                IsAuthorized = (bool)ClientTable["IsAuthorized"];
               return true;
            }
            catch
            { return false; }
        }

       public void SetAuthorizedCredentials(string Key, object Value)
        {
            Hashtable ClientTable = (Hashtable)System.Web.HttpContext.Current.Session["UserInfo"];
            ClientTable[Key] = Value;
            System.Web.HttpContext.Current.Session["UserInfo"] = ClientTable;
        }
        #endregion
    }
}
